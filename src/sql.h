/**
 * @file
 * @brief Database-independent SQL database layer.
 */

#pragma once

#include <stddef.h>

/// @brief Internal database connection.
struct idbc;

/// @brief Internal database cursor.
struct idb_cursor;

/// @brief Database connection.
struct dbc {
	/// @brief Associated backend.
	struct db_backend *backend;
	/// @brief Size of the cursor list.
	size_t cursors_num;
	/// @brief Opened cursor list.
	struct db_cursor **cursors;
	/// @brief Internal object.
	struct idbc *idbc;
};

/// @brief Database cursor.
struct db_cursor {
	/// @brief Associated connection.
	struct dbc *dbc;
	/// @brief Internal object.
	struct idb_cursor *icur;
};

/// @brief SQL NULL.
#define SQL_NULL 0
/// @brief 32-bit integer.
#define SQL_INTEGER 1
/// @brief Unbounded variable-length character string.
#define SQL_TEXT 2

/// @brief SQL parameter.
struct db_param {
	/// @brief Type of the parameter.
	char type;
	/// @brief Value union.
	union {
		/// @brief String value.
		const char *str;
		/// @brief Numeric value.
		long num;
	} value;
};

/// @brief Database backend definition.
struct db_backend {
	struct idbc *(*f_db_open)(const char*);
	void (*f_db_close)(struct dbc*);
	int (*f_db_create_prepared)(struct dbc*, const char*, const char*);
	int (*f_db_delete_prepared)(struct dbc*, const char*);
	struct idb_cursor *(*f_db_exec_prepared)(struct dbc*, const char*,
	                                         size_t, struct db_param*);
	struct idb_cursor *(*f_db_exec_direct)(struct dbc*, const char*,
	                                       size_t, struct db_param*);
	void (*f_db_cursor_free)(struct db_cursor*);
	int (*f_db_cursor_bind)(struct db_cursor*, int, struct db_param*);
	int (*f_db_cursor_end)(struct db_cursor*);
	int (*f_db_cursor_next)(struct db_cursor*);
};

/// @brief Global database connection.
extern struct dbc *gdbc;

/**
 * @brief Register a database backend.
 * @param name Name.
 * @param backend Backend structure.
 * @return 0 if successful, or -1 in case of error.
 */
int db_register_backend(const char *name, struct db_backend *backend);

/**
 * @brief Deinitialize all internal structures.
 */
void db_exit();

/**
 * @brief Open global database connection.
 * @return 0 if successful, -1 in case of error.
 */
int db_open_global();

/**
 * @brief Close global database connection.
 */
void db_close_global();

/**
 * @brief Open a database connection.
 * @param backend Name of the backend.
 * @param connstring Connection string to pass to the backend.
 * @return Database connection, or NULL in case of error.
 */
struct dbc *db_open(const char *backend, const char *connstring);

/**
 * @brief Close a database connection.
 * @param dbc Database connection.
 */
void db_close(struct dbc *dbc);

/**
 * @brief Create an SQL prepared statement.
 * @param dbc Database connection.
 * @param name Name of the statement.
 * @param stmt Statement.
 * @return 0 if successful, -1 in case of error.
 */
int db_create_prepared(struct dbc *dbc, const char *name, const char *stmt);

/**
 * @brief Delete an SQL prepared statement.
 * @param dbc Database connection.
 * @param name Name of the statement.
 * @return 0 if successful, -1 in case of error.
 */
int db_delete_prepared(struct dbc *dbc, const char *name);

/**
 * @brief Execute an SQL prepared statement.
 * @param dbc Database connection.
 * @param name Name of the statement.
 * @param params_num Number of parameters.
 * @param params Parameters.
 * @return Database cursor, or NULL in case of error.
 */
struct db_cursor *db_exec_prepared(struct dbc *dbc, const char *name,
                                   size_t params_num, struct db_param *params);

/**
 * @brief Execute an SQL statement.
 * @param dbc Database connection.
 * @param stmt Statement.
 * @param params_num Number of parameters.
 * @param params Parameters.
 * @return Database cursor, or NULL in case of error.
 */
struct db_cursor *db_exec_direct(struct dbc *dbc, const char *stmt,
                                 size_t params_num, struct db_param *params);

/**
 * @brief Free a database cursor.
 * @param cur Database cursor.
 */
void db_cursor_free(struct db_cursor *cur);

/**
 * @brief Bind a parameter to a column in a database cursor.
 * @param cur Database cursor.
 * @param column Index of column, starting from 0.
 * @param param Pointer to parameter.
 * @return 0 if successful, or -1 in case of error.
 */
int db_cursor_bind(struct db_cursor *cur, int column, struct db_param *param);

/**
 * @brief Check if there are still more rows.
 * @param cur Database cursor.
 * @return 0 if there are more rows, 1 if there aren't any more rows.
 */
int db_cursor_end(struct db_cursor *cur);

/**
 * @brief Retrieve the next row in a database cursor.
 * @param cur Database cursor.
 * @return 0 if successful, or -1 in case of error.
 */
int db_cursor_next(struct db_cursor *cur);
