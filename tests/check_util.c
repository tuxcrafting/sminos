#include <check.h>

#include <config.h>
#include <stddef.h>
#include <string.h>

#include "util.h"

#ifndef ck_assert_mem_eq
#define ck_assert_mem_eq(a, b, n) ck_assert(memcmp(a, b, n) == 0)
#endif

#ifndef HAVE_STRLCPY
START_TEST(check_strlcpy)
{
	char buf[8];

	memset(buf, 0, 8);
	ck_assert_uint_eq(strlcpy(buf, "hello", 8), 5);
	ck_assert_mem_eq(buf, "hello\0\0\0", 8);

	memset(buf, 0, 8);
	ck_assert_uint_eq(strlcpy(buf, "helloworld", 8), 10);
	ck_assert_mem_eq(buf, "hellowo\0", 8);

	memset(buf, 1, 8);
	ck_assert_uint_eq(strlcpy(buf, "helloworld", 0), 10);
	ck_assert_mem_eq(buf, "hellowo\0", 8);
}
END_TEST
#endif

START_TEST(check_saprintf)
{
	ck_assert_str_eq(saprintf("Hello, %s!", "World"), "Hello, World!");
	ck_assert_int_eq(saprintf("")[1], 'e');
	char test[8193];
	memset(test, 'a', 8192);
	test[8192] = 0;
	const char *s;
	ck_assert((s = saprintf("%s", test)) != NULL);
	ck_assert_uint_eq(smemlen(s), 8192 + 512);
	ck_assert_str_eq((s = saprintf("a")), "a");
	ck_assert_uint_eq(smemlen(s), 8192);
	saprintf(NULL);
}
END_TEST

START_TEST(check_path_join)
{
	ck_assert_str_eq(path_join("hello", NULL), "hello");
	ck_assert_str_eq(path_join("/hello", NULL), "/hello");
	ck_assert_str_eq(path_join("hello/", NULL), "hello/");

	ck_assert_str_eq(path_join("hello", "world", NULL), "hello/world");
	ck_assert_str_eq(path_join("hello/", "world", NULL), "hello/world");
	ck_assert_str_eq(path_join("hello", "/world", NULL), "hello/world");
	ck_assert_str_eq(path_join("hello/", "/world", NULL), "hello/world");

	ck_assert_str_eq(
		path_join("/a", "b", "/c/", "d/", "e/", "/f", "g/", NULL),
		"/a/b/c/d/e/f/g/");
}
END_TEST

int main() {
	Suite *suite = suite_create("util");
	TCase *tcase = tcase_create("core");

#ifndef HAVE_STRLCPY
	tcase_add_test(tcase, check_strlcpy);
#endif
	tcase_add_test(tcase, check_saprintf);
	tcase_add_test(tcase, check_path_join);
	suite_add_tcase(suite, tcase);

	SRunner *srunner = srunner_create(suite);
	srunner_run_all(srunner, CK_NORMAL);
	int failed = srunner_ntests_failed(srunner);

	srunner_free(srunner);
	return failed == 0 ? 0 : 1;
}
