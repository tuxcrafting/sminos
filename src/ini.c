#include "ini.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

char *ini_find_in_section(struct ini_section *section, const char *key) {
	if (key == NULL) {
		return NULL;
	}

	for (size_t i = 0; section->pairs[i] != NULL; i += 2) {
		if (strcmp(section->pairs[i], key) == 0) {
			return section->pairs[i + 1];
		}
	}

	return NULL;
}

struct ini_section *ini_find_section(struct ini *ini, const char *section) {
	if (section == NULL) {
		return &ini->global;
	}

	for (size_t i = 0; ini->sections[i].name != NULL; i++) {
		if (strcmp(ini->sections[i].name, section) == 0) {
			return &ini->sections[i];
		}
	}

	return NULL;
}

char *ini_find(struct ini *ini, const char *section, const char *key) {
	struct ini_section *s = ini_find_section(ini, section);
	if (s == NULL) {
		return NULL;
	}
	return ini_find_in_section(s, key);
}

char *ini_new_in_section(struct ini_section *section, const char *key, const char *value) {
	size_t len;
	for (len = 0; section->pairs[len] != NULL; len++) {}
	len++;
	section->pairs = srealloc(section->pairs, len + 2, sizeof(char*));

	section->pairs[len - 1] = sstrdup(key);
	section->pairs[len] = sstrdup(value);
	section->pairs[len + 1] = NULL;

	return section->pairs[len];
}

struct ini_section *ini_new_section(struct ini *ini, const char *section) {
	size_t len;
	for (len = 0; ini->sections[len].name != NULL; len++) {}
	len++;
	ini->sections = srealloc(ini->sections, len + 1, sizeof(struct ini_section));

	ini->sections[len - 1].name = sstrdup(section);
	ini->sections[len - 1].pairs = salloc(1, sizeof(char*));
	ini->sections[len].name = NULL;

	return &ini->sections[len - 1];
}

static int skip_whitespace(int ic, int (*getc)(void*), void *arg) {
	if (!isblank(ic)) {
		return ic;
	}
	int c;
	while (isblank(c = getc(arg))) {}
	return c;
}

static struct ini *_ini_parse(int (*getc)(void*), void *arg, int *is_error, char *err) {
	struct ini *ini = salloc(1, sizeof(struct ini));

	ini->global.pairs = salloc(1, sizeof(char*));
	ini->sections = salloc(1, sizeof(struct ini_section));

#define _ERROR(s)	  \
	{ \
		ini_free(ini); \
		if (err != NULL) { \
			sprintf(err, "line %d: " s, line); \
		} \
		return NULL; \
	}

#define _SAFE(c, v, eof, ...)	  \
	{ \
		c = (v); \
		if (c < 0) { \
			if (*is_error) { \
				__VA_ARGS__; \
				_ERROR("read error"); \
			} \
			if (eof == 1) { \
				__VA_ARGS__; \
				break; \
			} else if (eof == 0) { \
				__VA_ARGS__; \
				_ERROR("unexpected eof"); \
			} \
		} \
	}

#define _SECTION_LEN 128
#define _KEY_LEN 128
#define _VALUE_LEN 128

	struct ini_section *section = &ini->global;
	int line = 1;
	int c;
	for (;;) {
		c = ' ';
		_SAFE(c, skip_whitespace(c, getc, arg), 1);
		if (c == '\n') {
			continue;
		} else if (c == ';') {
			while (c != '\n') {
				_SAFE(c, getc(arg), 2);
				if (c < 0) {
					return ini;
				}
			}
		} else if (c == '[') {
			_SAFE(c, getc(arg), 0);
			_SAFE(c, skip_whitespace(c, getc, arg), 0);
			if (isalnum(c) || c == '_') {
				size_t i = 0;
				char *section_name = salloc(_SECTION_LEN, 1);
				while (isalnum(c) || c == '_') {
					if (i == _SECTION_LEN - 1) {
						sfree(section_name);
						_ERROR("section name too long");
					}
					section_name[i++] = c;
					_SAFE(c, getc(arg), 0, sfree(section_name));
				}

				_SAFE(c, skip_whitespace(c, getc, arg), 0, sfree(section_name));
				if (c != ']') {
					sfree(section_name);
					_ERROR("expected ']'");
				}

				if (ini_find_section(ini, section_name) != NULL) {
					sfree(section_name);
					_ERROR("section already exists");
				}

				section = ini_new_section(ini, section_name);
				sfree(section_name);
			} else {
				_ERROR("expected alphanumeric character");
			}
		} else if (isalnum(c) || c == '_') {
			size_t i = 0;
			char *key = salloc(_KEY_LEN, 1);
			while (isalnum(c) || c == '_') {
				if (i == _KEY_LEN - 1) {
					sfree(key);
					_ERROR("key too long");
				}
				key[i++] = c;
				_SAFE(c, getc(arg), 0, sfree(key));
			}

			if (ini_find_in_section(section, key) != NULL) {
				sfree(key);
				_ERROR("key already exists");
			}

			_SAFE(c, skip_whitespace(c, getc, arg), 0, sfree(key));
			if (c != '=') {
				sfree(key);
				_ERROR("expected '='");
			}

			char *value;
			_SAFE(c, getc(arg), 0, sfree(key));
			_SAFE(c, skip_whitespace(c, getc, arg), 2, sfree(key));
			if (c < 0) {
				value = salloc(1, 1);
			} else {
				size_t len = _VALUE_LEN;
				value = salloc(_VALUE_LEN, 1);
				i = 0;
				while (c != '\n' && c >= 0) {
					if (i == len - 1) {
						len += _VALUE_LEN;
						value = srealloc_n(value, len);
					}
					char xc = c;
					if (c == '\\') {
						_SAFE(c, getc(arg), 0, sfree(key); sfree(value));
						switch (c) {
						case '\\': xc = '\\'; break;
						case 'a': xc = '\a'; break;
						case 'b': xc = '\b'; break;
						case 'e': xc = 0x1B; break;
						case 'f': xc = '\f'; break;
						case 'n': xc = '\n'; break;
						case 'r': xc = '\r'; break;
						case 't': xc = '\t'; break;
						case 'v': xc = '\v'; break;
						case 'x': {
							int n = 0;
							for (int i = 0; i < 2; i++) {
								_SAFE(c, getc(arg), 2,
								      sfree(key); sfree(value));
								n *= 16;
								if (c >= '0' && c <= '9') {
									n += c - '0';
								} else if (c >= 'A' && c <= 'F') {
									n += (c - 'A') + 10;
								} else if (c >= 'a' && c <= 'f') {
									n += (c - 'a') + 10;
								} else {
									sfree(key);
									sfree(value);
									_ERROR("invalid character"
									       " in hex");
								}
							}
							xc = n;
							break;
						}
						default:
							sfree(key);
							sfree(value);
							_ERROR("invalid escape sequence");
							break;
						}
					}
					value[i++] = xc;
					_SAFE(c, getc(arg), 2, sfree(key); sfree(value));
				}
				value[i] = 0;
			}

			ini_new_in_section(section, key, value);
			sfree(key);
			sfree(value);
		} else {
			_ERROR("expected '[' or alphanumeric character");
		}
		if (c != '\n') {
			_SAFE(c, getc(arg), 1);
		}
		_SAFE(c, skip_whitespace(c, getc, arg), 1);
		if (c != '\n') {
			_ERROR("expected end of line");
		}
		line++;
	}

	return ini;
}

static int ini_parse_getc(void *arg) {
	struct {
		FILE *stream;
		int is_error;
	} *data = arg;
	int c = fgetc(data->stream);
	if (c == -1 && ferror(data->stream)) {
		data->is_error = 1;
	}
	return c;
}

struct ini *ini_parse(FILE *stream, char *err) {
	struct {
		FILE *stream;
		int is_error;
	} data;
	data.stream = stream;
	data.is_error = 0;
	return _ini_parse(ini_parse_getc, &data, &data.is_error, err);
}

static int ini_parse_string_getc(void *arg) {
	struct {
		const char *str;
		size_t i;
	} *data = arg;
	if (data->str[data->i] == 0) {
		return -1;
	}
	return data->str[data->i++];
}

struct ini *ini_parse_string(const char *str, char *err) {
	struct {
		const char *str;
		size_t i;
	} data;
	int is_error = 0;
	data.str = str;
	data.i = 0;
	return _ini_parse(ini_parse_string_getc, &data, &is_error, err);
}

static void ini_free_section(struct ini_section *section) {
	sfree(section->name);
	for (size_t i = 0; section->pairs[i] != NULL; i++) {
		sfree(section->pairs[i]);
	}
	sfree(section->pairs);
}

void ini_free(struct ini *ini) {
	if (ini == NULL) {
		return;
	}
	ini_free_section(&ini->global);
	for (size_t i = 0; ini->sections[i].name != NULL; i++) {
		ini_free_section(&ini->sections[i]);
	}
	sfree(ini->sections);
	sfree(ini);
}
