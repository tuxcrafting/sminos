#include "trie.h"

#include <stdlib.h>
#include <string.h>

#include "util.h"

struct trie *trie_new() {
	struct trie *trie = salloc(1, sizeof(struct trie));
	trie->root = 1;
	return trie;
}

void trie_free(struct trie *trie, void (*func)(void*, void*), void *func_arg) {
	if (!trie->root && trie->character == 0 && func != NULL) {
		func(trie->data, func_arg);
	} else if (trie->children_num != 0) {
		for (size_t i = 0; i < trie->children_num; i++) {
			trie_free(trie->children[i], func, func_arg);
		}
		sfree(trie->children);
	}
	sfree(trie);
}

int trie_add(struct trie *trie, const char *word, void *data) {
	struct trie *state = trie;
	size_t i;
	for (i = 0;; i++) {
		int found = 0;
		for (size_t j = 0; j < state->children_num; j++) {
			if (state->children[j]->character == word[i]) {
				state = state->children[j];
				found = 1;
				break;
			}
		}
		if (!found) {
			break;
		}
		if (word[i] == 0) {
			return -1;
		}
	}

	for (;; i++) {
		state->children = srealloc(state->children,
		                           state->children_num + 1, sizeof(struct trie*));

		struct trie *child = salloc(1, sizeof(struct trie));
		child->character = word[i];
		state->children[state->children_num++] = child;
		state = child;

		if (word[i] == 0) {
			state->data = data;
			return 0;
		}
	}
}

static struct trie *find_branch(struct trie *trie, const char *word, int *stick) {
	for (size_t i = 0; i < trie->children_num; i++) {
		if (trie->children[i]->character == word[0]) {
			if (word[0] == 0) {
				return trie;
			}
			struct trie *state = find_branch(trie->children[i], &word[1], stick);
			if (state == NULL) {
				return NULL;
			}
			if (trie->children_num == 1 && *stick == 0) {
				return trie;
			} else {
				if (*stick == 0) {
					memmove(&trie->children[i], &trie->children[i + 1],
					        (trie->children_num - i - 1) *
					        sizeof(struct trie*));
					trie->children_num--;
				}
				*stick = 1;
				return state;
			}
		}
	}
	return NULL;
}

int trie_delete(struct trie *trie, const char *word, void (*func)(void*, void*), void *func_arg) {
	int stick = 0;
	struct trie *state = find_branch(trie, word, &stick);
	if (state == NULL) {
		return -1;
	}
	trie_free(state, func, func_arg);
	return 0;
}

int trie_search(struct trie *trie, const char *word, void **data) {
	struct trie *state = trie;
	for (size_t i = 0;; i++) {
		int found = 0;
		for (size_t j = 0; j < state->children_num; j++) {
			if (state->children[j]->character == word[i]) {
				state = state->children[j];
				found = 1;
				break;
			}
		}
		if (!found) {
			return -1;
		}
		if (word[i] == 0) {
			break;
		}
	}

	if (data != NULL) {
		*data = state->data;
	}
	return 0;
}
