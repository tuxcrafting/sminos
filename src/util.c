#include "util.h"

#include <config.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef HAVE_STRLCPY
size_t strlcpy(char *dst, const char *src, size_t size) {
	if (size > 0) {
		strncpy(dst, src, size - 1);
		dst[size - 1] = 0;
	}
	return strlen(src);
}
#endif

static char *gsaprintfbuf;

const char *saprintf_r(char **buf, const char *format, ...) {
	va_list ap;
	va_start(ap, format);

	if (buf == NULL) {
		buf = &gsaprintfbuf;
	}
	if (format == NULL) {
		sfree(*buf);
		va_end(ap);
		return NULL;
	}

	if (*buf == NULL) {
		*buf = salloc_n(4096);
	}
	if (smemlen(*buf) >= 8192) {
		*buf = srealloc_n(*buf, 8192);
	}

	for (;;) {
		va_list ac;
		va_copy(ac, ap);
		int n = vsnprintf(*buf, smemlen(*buf), format, ac);
		va_end(ac);
		if (n >= 0 && n < smemlen(*buf)) {
			break;
		}
		*buf = srealloc_n(*buf, smemlen(*buf) + 512);
	}

	va_end(ap);
	return *buf;
}

const char *path_join(const char *base, ...) {
	va_list ap;
	va_start(ap, base);

#define BUFFER_SIZE 2048
	static char buffer[BUFFER_SIZE];

	size_t i = strlcpy(buffer, base, BUFFER_SIZE);
	if (i >= BUFFER_SIZE) {
		return NULL;
	}

	const char *s;
	while ((s = va_arg(ap, const char*)) != NULL) {
		if (i > 0 && buffer[i - 1] != '/' && s[0] != '/') {
			if (i >= BUFFER_SIZE - 1) {
				return NULL;
			}

			buffer[i++] = '/';
		} else if (i > 0 && buffer[i - 1] == '/' && s[0] == '/') {
			buffer[--i] = 0;
		}

		size_t s_l = strlcpy(&buffer[i], s, BUFFER_SIZE - i);

		if (s_l >= BUFFER_SIZE - i) {
			return NULL;
		}

		i += s_l;
	}

	va_end(ap);
	return buffer;
}

void *salloc_n(size_t len) {
	void *ptr = malloc(len + sizeof(size_t));
	if (ptr == NULL) {
		fprintf(stderr, "failed to allocate %zu bytes\n", len);
		abort();
	}

	*(size_t*)ptr = len;
	memset(ptr + sizeof(size_t), 0, len);
	return ptr + sizeof(size_t);
}

void *srealloc_n(void *ptr, size_t len) {
	if (ptr == NULL) {
		return salloc_n(len);
	}

	size_t old = smemlen(ptr);
	ptr = realloc(ptr - sizeof(size_t), len + sizeof(size_t));
	if (ptr == NULL) {
		fprintf(stderr, "failed to reallocate from %zu to %zu bytes\n", old, len);
		abort();
	}

	*(size_t*)ptr = len;
	if (len > old) {
		memset(ptr + sizeof(size_t) + old, 0, len - old);
	}
	return ptr + sizeof(size_t);
}

char *sstrdup(const char *str) {
	return smemdup_n(str, strlen(str) + 1);
}

void *smemdup_n(const void *ptr, size_t len) {
	void *p = salloc_n(len);
	memcpy(p, ptr, len);
	return p;
}

void sfree(void *ptr) {
	if (ptr != NULL) {
		size_t len = smemlen(ptr);
		memset(ptr, 0, len);
		free(ptr - sizeof(size_t));
	}
}
