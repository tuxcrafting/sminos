#include <event2/buffer.h>
#include <event2/http.h>

#include "eventloop.h"

static int index_redirect_route(struct evhttp_request *req,
                                struct evhttp_uri *uri,
                                struct evbuffer *evb) {
	evhttp_add_header(evhttp_request_get_output_headers(req),
	                  "Location", "/web");
	return 301;
}

static int index_route(struct evhttp_request *req,
                       struct evhttp_uri *uri,
                       struct evbuffer *evb) {
	evhttp_add_header(evhttp_request_get_output_headers(req),
	                  "Content-Type", "text/html");
	evbuffer_add_printf(evb, "<p>Hello, World!</p>");
	return 200;
}

void plugin_init() {
	http_register_route(EVHTTP_REQ_GET, "/", index_redirect_route);
	http_register_route(EVHTTP_REQ_GET, "/web", index_route);
}
