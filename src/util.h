/**
 * @file
 * @brief Various utility functions.
 */

#pragma once

#include <stddef.h>

/**
 * @brief {Open|Free}BSD's strlcpy(3).
 * @param dst Destination string.
 * @param src Source string.
 * @param size Size of buffer, including NUL byte.
 * @return Length of the source string.
 */
size_t strlcpy(char *dst, const char *src, size_t size);

/**
 * @brief sprintf()-like function that automatically allocates space for the string.
 * @param format printf() format. If NULL, deallocates the buffer.
 * @param ... Format arguments.
 * @return Formatted string.
 */
#define saprintf(format, ...) saprintf_r(NULL, format, ## __VA_ARGS__)

/**
 * @brief sprintf()-like function that automatically allocates space for the string.
 * @param buf Buffer to use. If NULL, uses a default static one.
 * @param format printf() format. If NULL, deallocates the buffer.
 * @param ... Format arguments.
 * @return Formatted string.
 */
const char *saprintf_r(char **buf, const char *format, ...);

/**
 * @brief Join paths.
 * @param base Base string.
 * @param ... Strings, finished with NULL.
 * @return Static buffer containing the joined paths, or NULL in case of error.
 */
const char *path_join(const char *base, ...);

/**
 * @brief Return length of memory area allocated with salloc_n() or srealloc_n().
 * @param ptr Pointer.
 * @return Length in bytes.
 */
#define smemlen(ptr) (*(size_t*)((ptr) - sizeof(size_t)))

/**
 * @brief Convenience wrapper over salloc_n().
 * @param nmemb Number of elements to allocate.
 * @param size Size of each element in bytes.
 * @return Allocated pointer.
 */
#define salloc(nmemb, size) (salloc_n((nmemb) * (size)))

/**
 * @brief Allocate data. Allocated pointer will be zeroed. abort()'s if allocation fails.
 * @param len Number of bytes to allocate.
 * @return Allocated pointer.
 */
void *salloc_n(size_t len);

/**
 * @brief Convenience wrapper over salloc_n().
 * @param ptr Pointer allocated with salloc_n() or srealloc_n(). If NULL, will be newly allocated.
 * @param nmemb Number of elements to allocate.
 * @param size Size of each element in bytes.
 * @return Allocated pointer.
 */
#define srealloc(ptr, nmemb, size) (srealloc_n((ptr), (nmemb) * (size)))

/**
 * @brief Reallocate data. Newly allocated memory will be zeroed. abort()'s if allocation fails.
 * @param ptr Pointer allocated with salloc_n() or srealloc_n(). If NULL, will be newly allocated.
 * @param len Number of bytes to allocate.
 * @return Reallocated pointer.
 */
void *srealloc_n(void *ptr, size_t len);

/**
 * @brief Duplicate a string.
 * @param str NUL-terminated string.
 * @return salloc_n() allocated copy.
 */
char *sstrdup(const char *str);

/**
 * @brief Convenience wrapper over smemdup_n().
 * @param ptr Memory area.
 * @param nmemb Number of elements in the area.
 * @param size Size of each element in bytes.
 * @return salloc_n() allocated copy.
 */
#define smemdup(ptr, nmemb, size) (smemdup_n((ptr), (nmemb) * (size)))

/**
 * @brief Duplicate a memory area.
 * @param ptr Memory area.
 * @param len Length of the memory area in bytes.
 * @return salloc_n() allocated copy.
 */
void *smemdup_n(const void *ptr, size_t len);

/**
 * @brief Zero and free data allocated with salloc_n() or srealloc_n().
 * @param ptr Pointer. Does nothing if this is NULL.
 */
void sfree(void *ptr);
