#include <check.h>

#include <stdio.h>
#include <unistd.h>

#include "ini.h"

#define m_ini_parse_string(d, s)	  \
	{ \
		d = ini_parse_string(s, NULL); \
		ck_assert(d != NULL); \
	}

#define assert_ini_parse(s)	  \
	{ \
		char err[256]; \
		struct ini *ini = ini_parse_string(s, err); \
		ck_assert_msg(ini != NULL, err); \
		ini_free(ini); \
	}
#define assert_ini_not_parse(s)	  \
	ck_assert(ini_parse_string(s, NULL) == NULL);

START_TEST(check_ini_parse)
{
	assert_ini_parse("");
	assert_ini_parse("\n");
	assert_ini_parse("; this is a comment");
	assert_ini_parse("; this is a comment\n");
	assert_ini_parse("[section]");
	assert_ini_parse("[section]\n");
	assert_ini_parse("[section1]\n[section2]");
	assert_ini_parse("key = value");
	assert_ini_parse("key = value\n");
	assert_ini_parse("key = value space");
	assert_ini_parse("a = b\n[section]\nc = d");
	assert_ini_parse("a = b\n[section]\nc = d\ne = f");
	assert_ini_parse("test = \\n");
	assert_ini_parse("test = \\t");
	assert_ini_parse("test = \\\\");
	assert_ini_parse("test = \\xAa");
	assert_ini_parse("test = \\x88");

	assert_ini_not_parse(",");
	assert_ini_not_parse("[section space]");
	assert_ini_not_parse("[$section]");
	assert_ini_not_parse("key space = value");
	assert_ini_not_parse("$key = value");
	assert_ini_not_parse("key ! value");
	assert_ini_not_parse("test\\n = 1");
	assert_ini_not_parse("test = \\");
	assert_ini_not_parse("test = \\l");
	assert_ini_not_parse("test = \\xp");
	assert_ini_not_parse("test = \\x1");
	assert_ini_not_parse("test = \\xpp");
	assert_ini_not_parse("a = 1\na = 2");
	assert_ini_not_parse("[section]\n[section]");
}
END_TEST

START_TEST(check_ini_find)
{
	struct ini *ini = NULL;

	m_ini_parse_string(ini, "");
	ck_assert(ini_find_section(ini, NULL) != NULL);
	ck_assert(ini_find_section(ini, "nonexistent") == NULL);

	m_ini_parse_string(ini, "a = b\n[section]\nc = d");
	ck_assert(ini_find_section(ini, NULL) != NULL);
	ck_assert(ini_find_section(ini, "section") != NULL);
	ck_assert(ini_find_section(ini, "nonexistent") == NULL);
	ck_assert_str_eq(ini_find(ini, NULL, "a"), "b");
	ck_assert(ini_find(ini, NULL, "c") == NULL);
	ck_assert(ini_find(ini, NULL, "aa") == NULL);
	ck_assert(ini_find(ini, "section", "a") == NULL);
	ck_assert_str_eq(ini_find(ini, "section", "c"), "d");
	ck_assert(ini_find(ini, "section", "aa") == NULL);
	ck_assert(ini_find(ini, "nonexistent", "a") == NULL);
	ck_assert(ini_find(ini, "nonexistent", "c") == NULL);
	ck_assert(ini_find(ini, "nonexistent", "aa") == NULL);

	m_ini_parse_string(ini,
	                   "var0 = hello\n"
	                   "var1 = world\n"
	                   "[section1]\n"
	                   "var0 = foo\n"
	                   "var1 = bar\n"
	                   "var2 = baz\n"
	                   "[section2]\n"
	                   "var0 = value\n");
	ck_assert_str_eq(ini_find(ini, "section1", "var1"), "bar");
	ck_assert_str_eq(ini_find(ini, "section1", "var2"), "baz");
	ck_assert(ini_find(ini, "section2", NULL) == NULL);
	ck_assert(ini_find(ini, "section3", "var1") == NULL);
	ck_assert_str_eq(ini_find(ini, "section1", "var0"), "foo");
	ck_assert(ini_find(ini, "section3", "var0") == NULL);
	ck_assert(ini_find(ini, "section1", "var3") == NULL);
	ck_assert_str_eq(ini_find(ini, "section2", "var0"), "value");
	ck_assert_str_eq(ini_find(ini, NULL, "var1"), "world");
	ck_assert_str_eq(ini_find(ini, NULL, "var0"), "hello");

	m_ini_parse_string(ini,
	                   "key1 = \\a\\b\\e\\f\\n\\r\\t\\v\\\\\n"
	                   "key2 = \\x88\\xAC\\xFb");
	ck_assert_str_eq(ini_find(ini, NULL, "key1"), "\a\b\x1B\f\n\r\t\v\\");
	ck_assert_str_eq(ini_find(ini, NULL, "key2"), "\x88\xAC\xFB");

	ini_free(ini);
}
END_TEST

int main() {
	Suite *suite = suite_create("ini");
	TCase *tcase = tcase_create("base");

	tcase_add_test(tcase, check_ini_parse);
	tcase_add_test(tcase, check_ini_find);
	suite_add_tcase(suite, tcase);

	SRunner *srunner = srunner_create(suite);
	srunner_run_all(srunner, CK_NORMAL);
	int failed = srunner_ntests_failed(srunner);

	srunner_free(srunner);
	return failed == 0 ? 0 : 1;
}
