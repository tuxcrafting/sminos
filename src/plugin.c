#include "plugin.h"

#include <event2/http.h>
#include <ltdl.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "conf.h"
#include "log.h"
#include "util.h"

static size_t plugin_num;
static lt_dlhandle *plugins;
static int finalized;
static lt_dladvise advise;

void plugin_init() {
	lt_dladvise_init(&advise);
	lt_dladvise_global(&advise);
	lt_dladvise_ext(&advise);
}

int plugin_load(const char *name) {
	lt_dlhandle plugin = lt_dlopenadvise(path_join(config.plugin.plugindir, name, NULL),
	                                     advise);
	if (plugin == NULL) {
		mlog(LEVEL_ERROR, "Failed to load plugin %s: %s", name, lt_dlerror());
		return -1;
	}

	plugins = srealloc(plugins, plugin_num + 1, sizeof(lt_dlhandle));
	plugins[plugin_num++] = plugin;

	return 0;
}

void plugin_finalize_load() {
	lt_dladvise_destroy(&advise);
	for (size_t i = 0; i < plugin_num; i++) {
		void (*constructor)() = lt_dlsym(plugins[i], "plugin_init");
		if (constructor != NULL) {
			constructor();
		}
	}
	finalized = 1;
}

void plugin_unload_all() {
	for (size_t i = 0; i < plugin_num; i++) {
		if (finalized) {
			void (*destructor)() = lt_dlsym(plugins[i], "plugin_exit");
			if (destructor != NULL) {
				destructor();
			}
		}
		lt_dlclose(plugins[i]);
	}
	sfree(plugins);
}
