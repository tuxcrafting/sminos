/**
 * @file
 * @brief Logging functions.
 */

#pragma once

#include <stddef.h>

/// @brief File logging target.
#define TARGET_FILE 1
/// @brief Standard output.
#define TARGET_STDOUT 2
/// @brief Standard error.
#define TARGET_STDERR 3
/// @brief syslog logging target.
#define TARGET_SYSLOG 4

/// @brief Lowest level message.
#define LEVEL_LOW 0
/// @brief Normal message.
#define LEVEL_NORMAL 1
/// @brief Warning message.
#define LEVEL_WARNING 2
/// @brief Error message.
#define LEVEL_ERROR 3
/// @brief Fatal error message.
#define LEVEL_FATAL 4

/// @brief Logging target.
struct log_target {
	/// @brief Type.
	int type;

	/// @brief Parameters.
	union {
		/// @brief Parameters for a file logging target.
		struct {
			/// @brief Path of the file.
			const char *path;
		} file;

		/// @brief Parameters for a syslog logging target.
		struct {
			/// @brief syslog identifier.
			const char *ident;
		} syslog;
	};
};

/**
 * @brief Initialize the logging state.
 * @param num Number of targets.
 * @param targets Logging targets. Only one syslog target can be specified.
 * @return 0 if successful, -1 in case of error.
 */
int log_init(size_t num, const struct log_target *targets);

/**
 * @brief Deinitialize the logging state.
 */
void log_deinit();

/**
 * @brief Log a message to the logging targets.
 * @param level Logging level.
 * @param format printf(3)-style format.
 * @param ... Arguments for the format string.
 */
void mlog(int level, const char *format, ...);
