#include "eventloop.h"

#include <event2/buffer.h>
#include <event2/event.h>
#include <event2/http.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "log.h"
#include "trie.h"
#include "util.h"

struct event_base *event_base;
struct evhttp *evhttp;
static struct evhttp_bound_socket *evhttp_sock;
static struct event *event_sigint;
static struct trie *path_trie;

struct path_trie_route {
	enum evhttp_cmd_type method;
	http_route_t handler;
};
struct path_trie_data {
	size_t routes_num;
	struct path_trie_route *routes;
};

static const char *method_name(enum evhttp_cmd_type method) {
	switch (method) {
	case EVHTTP_REQ_GET: return "GET";
	case EVHTTP_REQ_POST: return "POST";
	case EVHTTP_REQ_HEAD: return "HEAD";
	case EVHTTP_REQ_PUT: return "PUT";
	case EVHTTP_REQ_DELETE: return "DELETE";
	case EVHTTP_REQ_OPTIONS: return "OPTIONS";
	case EVHTTP_REQ_TRACE: return "TRACE";
	case EVHTTP_REQ_CONNECT: return "CONNECT";
	case EVHTTP_REQ_PATCH: return "PATCH";
	}
	return "UNKNOWN";
}

static void on_request(struct evhttp_request *req, void *arg) {
	enum evhttp_cmd_type method = evhttp_request_get_command(req);
	const char *uri = evhttp_request_get_uri(req);
	struct evhttp_uri *decoded = evhttp_uri_parse(uri);
	const char *query = evhttp_uri_get_query(decoded);
	const char *path = evhttp_uri_get_path(decoded);
	size_t path_len = strlen(path);
	if (path_len > 1 && path[path_len - 1] == '/') {
		((char*)path)[path_len - 1] = 0;
	}

	mlog(LEVEL_NORMAL, "%s %s%s%s",
	     method_name(method), path,
	     query == NULL ? "" : "?", query == NULL ? "" : query);

	void *_data;
	struct path_trie_data *data;
	if (trie_search(path_trie, path, &_data) == 0) {
		data = _data;
		int found = 0;
		for (size_t i = 0; i < data->routes_num; i++) {
			if (data->routes[i].method == method) {
				struct evbuffer *evb = evbuffer_new();
				int code = data->routes[i].handler(req, decoded, evb);
				if (code / 100 == 2 || code / 100 == 3) {
					evhttp_send_reply(req, code, NULL, evb);
				} else if (code != 0) {
					evhttp_send_error(req, code, NULL);
				}
				evbuffer_free(evb);

				found = 1;
				break;
			}
		}
		if (!found) {
			evhttp_send_error(req, HTTP_BADMETHOD, NULL);
		}
	} else {
		evhttp_send_error(req, HTTP_NOTFOUND, NULL);
	}

	evhttp_uri_free(decoded);
}

static void on_sigint(evutil_socket_t fd, short events, void *arg) {
	event_base_loopbreak(event_base);
}

int http_register_route(enum evhttp_cmd_type method, const char *path, http_route_t handler) {
	void *_data;
	struct path_trie_data *data;

	if (trie_search(path_trie, path, &_data) == 0) {
		data = _data;
		data->routes = srealloc(data->routes,
		                        data->routes_num + 1,
		                        sizeof(struct path_trie_route));
		data->routes[data->routes_num].method = method;
		data->routes[data->routes_num].handler = handler;
		data->routes_num++;
	} else {
		data = salloc(1, sizeof(struct path_trie_data));
		data->routes_num = 1;
		data->routes = salloc(1, sizeof(struct path_trie_route));
		data->routes[0].method = method;
		data->routes[0].handler = handler;

		if (trie_add(path_trie, path, data) < 0) {
			sfree(data->routes);
			sfree(data);
			return -1;
		}
	}

	return 0;
}

int eventloop_init() {
	event_base = event_base_new();
	if (event_base == NULL) {
		return -1;
	}

	evhttp = evhttp_new(event_base);
	if (evhttp == NULL) {
		event_base_free(event_base);
		return -1;
	}

	if (!dry) {
		evhttp_sock = evhttp_bind_socket_with_handle(evhttp,
		                                             config.server.address,
		                                             config.server.port);
		if (evhttp_sock == NULL) {
			evhttp_free(evhttp);
			event_base_free(event_base);
			mlog(LEVEL_ERROR, "Failed to bind HTTP server\n");
			return -1;
		}
	}

	evhttp_set_gencb(evhttp, on_request, NULL);

	event_sigint = event_new(event_base, SIGINT, EV_SIGNAL, on_sigint, NULL);
	if (event_sigint == NULL) {
		if (!dry) {
			evhttp_del_accept_socket(evhttp, evhttp_sock);
		}
		evhttp_free(evhttp);
		event_base_free(event_base);
		return -1;
	}

	path_trie = trie_new();

	if (event_add(event_sigint, NULL) < 0) {
		eventloop_quit();
		return -1;
	}

	return 0;
}

void eventloop_start() {
	event_base_loop(event_base, 0);
}

void free_path_trie_data(void *_data, void *arg) {
	struct path_trie_data *data = _data;
	sfree(data->routes);
	sfree(data);
}

void eventloop_quit() {
	if (!dry) {
		evhttp_del_accept_socket(evhttp, evhttp_sock);
	}
	evhttp_free(evhttp);
	event_del(event_sigint);
	event_free(event_sigint);
	event_base_free(event_base);
	trie_free(path_trie, free_path_trie_data, NULL);
}
