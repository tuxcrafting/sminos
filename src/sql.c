#include "sql.h"

#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "log.h"
#include "trie.h"
#include "util.h"

struct dbc *gdbc;

static struct trie *backend_trie;

int db_register_backend(const char *name, struct db_backend *backend) {
	if (backend_trie == NULL) {
		backend_trie = trie_new();
	}
	struct db_backend *c_backend = smemdup(backend, 1, sizeof(struct db_backend));
	if (trie_add(backend_trie, name, c_backend) < 0) {
		sfree(c_backend);
		return -1;
	}
	return 0;
}

static void free_backend(void *data, void *arg) {
	sfree(data);
}

void db_exit() {
	trie_free(backend_trie, free_backend, NULL);
}

int db_open_global() {
	gdbc = db_open(config.database.backend, config.database.connstring);
	if (gdbc == NULL) {
		return -1;
	}
	return 0;
}

void db_close_global() {
	db_close(gdbc);
}

struct dbc *db_open(const char *backend, const char *connstring) {
	void *data;
	if (trie_search(backend_trie, backend, &data) < 0) {
		return NULL;
	}
	struct db_backend *backend_v = data;
	struct dbc *dbc = salloc(1, sizeof(struct dbc));
	struct idbc *idbc = backend_v->f_db_open(connstring);
	if (idbc == NULL) {
		sfree(dbc);
		return NULL;
	}
	dbc->backend = backend_v;
	dbc->idbc = idbc;
	dbc->cursors_num = 8;
	dbc->cursors = salloc(8, sizeof(struct db_cursor*));
	return dbc;
}

void db_close(struct dbc *dbc) {
	for (size_t i = 0; i < dbc->cursors_num; i++) {
		if (dbc->cursors[i] != NULL) {
			db_cursor_free(dbc->cursors[i]);
		}
	}
	dbc->backend->f_db_close(dbc);
	sfree(dbc->cursors);
	sfree(dbc);
}

int db_create_prepared(struct dbc *dbc, const char *name, const char *stmt) {
	return dbc->backend->f_db_create_prepared(dbc, name, stmt);
}

int db_delete_prepared(struct dbc *dbc, const char *name) {
	return dbc->backend->f_db_delete_prepared(dbc, name);
}

static struct db_cursor *db_exec_g(struct dbc *dbc, const char *arg,
                                   size_t params_num, struct db_param *params,
                                   struct idb_cursor *(*func)(struct dbc*, const char*,
                                                              size_t, struct db_param*)) {
	struct db_cursor *cur = salloc(1, sizeof(struct db_cursor));
	struct idb_cursor *icur = func(dbc, arg, params_num, params);
	if (icur == NULL) {
		sfree(cur);
		return NULL;
	}
	cur->dbc = dbc;
	cur->icur = icur;
	int found = 0;
	for (size_t i = 0; i < dbc->cursors_num; i++) {
		if (dbc->cursors[i] == NULL) {
			dbc->cursors[i] = cur;
			found = 1;
			break;
		}
	}
	if (!found) {
		dbc->cursors = srealloc(dbc->cursors, dbc->cursors_num + 8,
		                        sizeof(struct db_cursor*));
		dbc->cursors[dbc->cursors_num] = cur;
		dbc->cursors_num += 8;
	}
	return cur;
}

struct db_cursor *db_exec_prepared(struct dbc *dbc, const char *name,
                                   size_t params_num, struct db_param *params) {
	return db_exec_g(dbc, name, params_num, params,
	                 dbc->backend->f_db_exec_prepared);
}

struct db_cursor *db_exec_direct(struct dbc *dbc, const char *stmt,
                                 size_t params_num, struct db_param *params) {
	return db_exec_g(dbc, stmt, params_num, params,
	                 dbc->backend->f_db_exec_direct);
}

void db_cursor_free(struct db_cursor *cur) {
	cur->dbc->backend->f_db_cursor_free(cur);
	for (size_t i = 0; i < cur->dbc->cursors_num; i++) {
		if (cur->dbc->cursors[i] == cur) {
			cur->dbc->cursors[i] = NULL;
			break;
		}
	}
	sfree(cur);
}

int db_cursor_bind(struct db_cursor *cur, int column, struct db_param *param) {
	return cur->dbc->backend->f_db_cursor_bind(cur, column, param);
}

int db_cursor_end(struct db_cursor *cur) {
	return cur->dbc->backend->f_db_cursor_end(cur);
}

int db_cursor_next(struct db_cursor *cur) {
	return cur->dbc->backend->f_db_cursor_next(cur);
}
