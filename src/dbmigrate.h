/**
 * @file
 * @brief Database migrations.
 */

/**
 * @brief Do the migrations.
 * @return 0 if successful, 1 in case of error.
 */
int do_migrate();
