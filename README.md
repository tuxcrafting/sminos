# Sminos

Sminos ([Smilodon](http://gitlab.com/tuxcrafting/smilodon)'s not [Smilodon](https://github.com/rowanlupton/smilodon)) (neither is it [Smilodon](https://source.puri.sm/liberty/smilodon)) is a federated microblogging platform.

## Building

Sminos uses the GNU Autotools, so building is just a matter of:

    $ autoreconf -i
    $ ./configure
    $ make

You can then run the tests with `make check`, or build the documentation with `make doc`.
