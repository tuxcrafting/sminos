/**
 * @file
 * @brief Global configuration.
 */

#pragma once

/// @brief Configuration struct.
struct conf_struct {
	/// @brief Internal HTTP server configuration.
	struct {
		/// @brief Listening address.
		char *address;
		/// @brief Listening port.
		unsigned short port;
	} server;

	/// @brief Logger configuration.
	struct {
		/**
		 * @brief Semicolon-separated list of logger targets.
		 *
		 * A target can be:
		 * - file:PATH - Log to a file specified by the path.
		 * - stdout - Log to standard output.
		 * - stderr - Log to standard error.
		 * - syslog:IDENT - Log to syslog with the specified identifier.
		 *
		 * Defaults to <code>stderr</code>.
		*/
		char *targets;
	} logger;

	/// @brief Plugin definitions.
	struct {
		/// @brief Directory where plugins are put.
		char *plugindir;

		/**
		 * @brief Semicolon-separated list of plugins.
		 *
		 * Plugins are searched for at PLUGINDIR/NAME.EXT,
		 * where EXT is the shared library extension of the current operating system
		 * (eg: .so for Unices, .dll for NT).
		 *
		 * Defaults to nothing.
		 */
		char *plugins;
	} plugin;

	/// @brief Database settings.
	struct {
		/// @brief Database backend.
		char *backend;

		/// @brief Connection string.
		char *connstring;
	} database;
};

/// @brief Instantiation of the configuration.
extern struct conf_struct config;

/// @brief If 1, no listeners will start.
extern int dry;

/**
 * @brief Load the configuration from a file.
 * @param path Path to the file.
 * @return 0 if successful, -1 on error.
 */
int conf_load(const char *path);

/**
 * @brief Unload the configuration.
 */
void conf_unload();
