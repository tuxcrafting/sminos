/**
 * @file
 * @brief Trie for efficient string searching.
 */

#pragma once

#include <stddef.h>

struct trie {
	/// @brief Character of the trie state.
	char character;

	/// @brief Whether the state is the root.
	char root;

	/// @brief Number of children.
	size_t children_num;

	union {
		/// @brief Array of children.
		struct trie **children;

		/// @brief Data associated with the string, if it is an end of word state.
		void *data;
	};
};

/**
 * @brief Create a new trie.
 * @return New trie.
 */
struct trie *trie_new();

/**
 * @brief Free a trie.
 * @param trie Trie.
 * @param func If not NULL, destructor function to run on the data.
 * @param func_arg Second argument of func.
 */
void trie_free(struct trie *trie, void (*func)(void*, void*), void *func_arg);

/**
 * @brief Add a word in a trie.
 * @param trie Trie.
 * @param word Word.
 * @param data Data associated with the word.
 * @return 0 if successful, -1 in case of error.
 */
int trie_add(struct trie *trie, const char *word, void *data);

/**
 * @brief Delete a trie branch.
 * @param trie Trie.
 * @param word Word.
 * @param func If not NULL, destructor function to run on the data.
 * @param func_arg Second argument of func.
 * @return 0 if successful, -1 in case of error.
 */
int trie_delete(struct trie *trie, const char *word, void (*func)(void*, void*), void *func_arg);

/**
 * @brief Search a word in a trie.
 * @param trie Trie.
 * @param word Word.
 * @param data If not NULL, pointer to put associated data in.
 * @return 0 if found, -1 if not found.
 */
int trie_search(struct trie *trie, const char *word, void **data);
