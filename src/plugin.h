/**
 * @file
 * @brief Plugin loading.
 */

#pragma once

/**
 * @brief Initialize the plugin subsystem.
 */
void plugin_init();

/**
 * @brief Load a plugin.
 * @param name Plugin name.
 * @return 0 if successful, -1 in case of error.
 */
int plugin_load(const char *name);

/**
 * @brief Finish loading the plugins.
 */
void plugin_finalize_load();

/**
 * @brief Unload all loaded plugins.
 */
void plugin_unload_all();
