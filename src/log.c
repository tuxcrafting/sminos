#include "log.h"

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>

#include "util.h"

static int to_syslog;

static size_t log_files_n;
static FILE **log_files;

int log_init(size_t num, const struct log_target *targets) {
	log_files = salloc(num, sizeof(FILE*));

	for (size_t i = 0; i < num; i++) {
		switch (targets[i].type) {
		case TARGET_FILE: {
			FILE *f = fopen(targets[i].file.path, "a+");
			if (f == NULL) {
				fprintf(stderr,
				        "Failed to initialize logging:"
				        "error while opening %s: %s",
				        targets[i].file.path, strerror(errno));
				log_deinit();
				return -1;
			}
			log_files[log_files_n++] = f;
			break;
		}
		case TARGET_STDOUT:
			log_files[log_files_n++] = stdout;
			break;
		case TARGET_STDERR:
			log_files[log_files_n++] = stderr;
			break;
		case TARGET_SYSLOG:
			if (to_syslog) {
				fprintf(stderr,
				        "Failed to initialize logging:"
				        " only one syslog target can be specified\n");
				log_deinit();
				return -1;
			}
			to_syslog = 1;
			openlog(targets[i].syslog.ident, 0, LOG_USER);
			break;
		}
	}

	return 0;
}

void log_deinit() {
	for (size_t i = 0; i < log_files_n; i++) {
		if (log_files[i] == stdout || log_files[i] == stderr) {
			continue;
		}
		fclose(log_files[i]);
	}
	sfree(log_files);
	if (to_syslog) {
		closelog();
	}
}

void mlog(int level, const char *format, ...) {
	va_list ap;
	va_start(ap, format);

	static char *level_strings[] = {
		"LOW", "NORMAL", "WARNING", "ERROR", "FATAL",
	};
	static int level_prio[] = {
		LOG_INFO, LOG_NOTICE, LOG_WARNING, LOG_ERR, LOG_CRIT,
	};

	time_t t = time(NULL);
	struct tm *tm = gmtime(&t);

	for (size_t i = 0; i < log_files_n; i++) {
		fprintf(log_files[i], "[%04d-%02d-%02dT%02d:%02d:%02dZ %s] ",
		        tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
		        tm->tm_hour, tm->tm_min, tm->tm_sec,
		        level_strings[level]);
		vfprintf(log_files[i], format, ap);
		fprintf(log_files[i], "\n");
	}

	if (to_syslog) {
		vsyslog(level_prio[level], format, ap);
	}

	va_end(ap);
}
