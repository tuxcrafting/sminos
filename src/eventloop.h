/**
 * @file
 * @brief Main event loop.
 */

#pragma once

#include <event2/buffer.h>
#include <event2/event.h>
#include <event2/http.h>

/// @brief HTTP route handler.
typedef int (*http_route_t)(struct evhttp_request*, struct evhttp_uri*, struct evbuffer*);

/// @brief Event loop object.
extern struct event_base *event_base;

/// @brief Internal HTTP server object.
extern struct evhttp *evhttp;

/**
 * @brief Register an HTTP route.
 * @param method HTTP method.
 * @param path Path.
 * @param handler Route handler.
 * @return 0 if successful, -1 in case of error.
 */
int http_register_route(enum evhttp_cmd_type method, const char *path, http_route_t handler);

/**
 * @brief Initialize the event loop.
 * @return 0 if successful, -1 in case of error.
 */
int eventloop_init();

/**
 * @brief Start the event loop.
 */
void eventloop_start();

/**
 * @brief Deinitialize the event loop.
 */
void eventloop_quit();
