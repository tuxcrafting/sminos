#include "conf.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ini.h"

struct conf_struct config;
int dry;
static struct ini *ini;

int conf_load(const char *path) {
	FILE *f = fopen(path, "r");
	if (f == NULL) {
		fprintf(stderr, "Failed opening file: %s\n", strerror(errno));
		return -1;
	}
	char err[512];
	ini = ini_parse(f, err);
	fclose(f);
	if (ini == NULL) {
		fprintf(stderr, "Failed parsing configuration: %s\n", err);
		return -1;
	}

#define FIND_M(dest, func, section, key)	  \
	{ \
		char *x = ini_find(ini, section, key); \
		if (x == NULL) { \
			fprintf(stderr, "Configuration property %s.%s not found", \
			        section == NULL ? "" : section, key); \
		} \
		dest = func; \
	}
#define FIND_O(dest, func, section, key, def)	  \
	{ \
		char *x = ini_find(ini, section, key); \
		if (x == NULL) { \
			dest = def; \
		} else { \
			dest = func; \
		} \
	}

	FIND_M(config.server.address, x, "server", "address");
	FIND_M(config.server.port, atoi(x), "server", "port");
	FIND_O(config.logger.targets, x, "logger", "targets", "stderr");
	FIND_M(config.plugin.plugindir, x, "plugin", "plugindir");
	FIND_O(config.plugin.plugins, x, "plugin", "plugins", "");
	FIND_M(config.database.backend, x, "database", "backend");
	FIND_M(config.database.connstring, x, "database", "connstring");

	return 0;
}

void conf_unload() {
	ini_free(ini);
}
