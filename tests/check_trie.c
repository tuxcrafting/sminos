#include <check.h>

#include "trie.h"

START_TEST(check_trie)
{
	struct trie *trie = trie_new();

	ck_assert_int_eq(trie_add(trie, "somebody", "once"), 0);
	ck_assert_int_eq(trie_add(trie, "told", "me"), 0);
	ck_assert_int_eq(trie_add(trie, "the", "world"), 0);
	ck_assert_int_eq(trie_add(trie, "was", "gonna"), 0);
	ck_assert_int_eq(trie_add(trie, "roll", "me,"), 0);
	ck_assert_int_eq(trie_add(trie, "the", "sharpest"), -1);

	void *data;

	ck_assert_int_eq(trie_search(trie, "the", &data), 0);
	ck_assert_str_eq(data, "world");
	ck_assert_int_eq(trie_search(trie, "roll", &data), 0);
	ck_assert_str_eq(data, "me,");
	ck_assert_int_eq(trie_search(trie, "tho", NULL), -1);
	ck_assert_int_eq(trie_search(trie, "ras", NULL), -1);
	ck_assert_int_eq(trie_search(trie, "somebodya", NULL), -1);
	ck_assert_int_eq(trie_search(trie, "rol", NULL), -1);

	ck_assert_int_eq(trie_delete(trie, "told", NULL, NULL), 0);
	ck_assert_int_eq(trie_delete(trie, "told", NULL, NULL), -1);
	ck_assert_int_eq(trie_delete(trie, "toad", NULL, NULL), -1);
	ck_assert_int_eq(trie_search(trie, "the", &data), 0);
	ck_assert_str_eq(data, "world");

	trie_free(trie, NULL, NULL);
}
END_TEST

int main() {
	Suite *suite = suite_create("trie");
	TCase *tcase = tcase_create("base");

	tcase_add_test(tcase, check_trie);
	suite_add_tcase(suite, tcase);

	SRunner *srunner = srunner_create(suite);
	srunner_run_all(srunner, CK_NORMAL);
	int failed = srunner_ntests_failed(srunner);

	srunner_free(srunner);
	return failed == 0 ? 0 : 1;
}
