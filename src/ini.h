/**
 * @file
 * @brief INI parser.
 */

#pragma once

#include <stdio.h>

/// @brief INI section.
struct ini_section {
	/// @brief Name of the section.
	char *name;

	/// @brief Key-value pairs, terminated with NULL.
	char **pairs;
};

/// @brief Parsed INI file.
struct ini {
	/// @brief Implicit top-level section.
	struct ini_section global;

	/// @brief Named sections, terminated with NULL.
	struct ini_section *sections;
};

/**
 * @brief Find a value in a section.
 * @param section Section.
 * @param key Key.
 * @return Value corresponding to the key in the section, or NULL if not found.
 */
char *ini_find_in_section(struct ini_section *section, const char *key);

/**
 * @brief Find a section in an INI file.
 * @param ini INI file.
 * @param section Section name, or NULL for top-level.
 * @return Section with specified name, or NULL if not found.
 */
struct ini_section *ini_find_section(struct ini *ini, const char *section);

/**
 * @brief Find a value in an INI file.
 * @param ini INI file.
 * @param section Section name, or NULL for top-level.
 * @param key Key.
 * @return Value corresponding to the key in the specified section, or NULL if not found.
 */
char *ini_find(struct ini *ini, const char *section, const char *key);

/**
 * @brief Create a key-value pair in a section. Does not check if key already exists.
 * @param section Section.
 * @param key Key.
 * @param value Value.
 * @return New value corresponding to the key in the section, or NULL in case of error.
 */
char *ini_new_in_section(struct ini_section *section, const char *key, const char *value);

/**
 * @brief Create a section in an INI file. Does not check if section already exists.
 * @param ini INI file.
 * @param section Section name, or NULL for top-level.
 * @return New section with specified name, or NULL in case of error.
 */
struct ini_section *ini_new_section(struct ini *ini, const char *section);

/**
 * @brief Parse an INI from a stream.
 * @param stream Stream.
 * @param err If not NULL, buffer to store errors in.
 * @return Parsed INI file, or NULL in case of error.
 */
struct ini *ini_parse(FILE *stream, char *err);

/**
 * @brief Parse an INI from a string.
 * @param str String..
 * @param err If not NULL, buffer to store errors in.
 * @return Parsed INI file, or NULL in case of error.
 */
struct ini *ini_parse_string(const char *str, char *err);

/**
 * @brief Free an INI file.
 * @param ini INI file.
 */
void ini_free(struct ini *ini);
