#include <config.h>
#include <event2/buffer.h>
#include <event2/http.h>
#include <getopt.h>
#include <ltdl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "dbmigrate.h"
#include "eventloop.h"
#include "log.h"
#include "plugin.h"
#include "sql.h"
#include "util.h"

static int init_n;

static void quit(int ret) {
	switch (init_n) {
	case 6:
		db_close_global();
		db_exit();
	case 5: plugin_unload_all();
	case 4: eventloop_quit();
	case 3: lt_dlexit();
	case 2: log_deinit();
	case 1: conf_unload();
	case 0: saprintf(NULL);
	}
	exit(ret);
}

static void print_usage(char **argv) {
	fprintf(stderr, "Usage: %s [-mhv] CONFIG\n", argv[0]);
}

static void print_help(char **argv) {
	print_usage(argv);
	fprintf(stderr, " CONFIG\tConfig file path\n");
	fprintf(stderr, " -m\tRun database migrations\n");
	fprintf(stderr, " -h\tPrint this help message and exit\n");
	fprintf(stderr, " -v\tPrint version information and exit\n");
}

int main(int argc, char *argv[]) {
	int mode = 0;

	char c;
	while ((c = getopt(argc, argv, "mhv")) >= 0) {
		switch (c) {
		case 'm':
			mode = 1;
			dry = 1;
			break;
		case 'h':
			print_help(argv);
			quit(0);
		case 'v':
			printf("%s\n", PACKAGE_STRING);
			quit(0);
		default:
			print_usage(argv);
			quit(1);
		}
	}

	if (optind != argc - 1) {
		print_usage(argv);
		quit(1);
	}

	if (conf_load(argv[optind++]) < 0) {
		quit(1);
	}
	init_n++;

	size_t semi_cnt = 0;
	for (size_t i = 0; config.logger.targets[i] != 0; i++) {
		if (config.logger.targets[i] == ';') {
			semi_cnt++;
		}
	}

	struct log_target *targets = NULL;
	if (config.logger.targets[0] != 0) {
		targets = salloc(semi_cnt + 1, sizeof(struct log_target));
		char *s = strtok(config.logger.targets, ";");
		for (size_t i = 0; s != NULL; i++) {
			if (strcmp(s, "stdout") == 0) {
				targets[i].type = TARGET_STDOUT;
			} else if (strcmp(s, "stderr") == 0) {
				targets[i].type = TARGET_STDERR;
			} else if (strstr(s, "file:") == s) {
				targets[i].type = TARGET_FILE;
				targets[i].file.path = &s[5];
			} else if (strstr(s, "syslog:") == s) {
				targets[i].type = TARGET_SYSLOG;
				targets[i].syslog.ident = &s[7];
			} else {
				sfree(targets);
				fprintf(stderr, "Failed to parse logger target: %s", s);
				quit(1);
			}
			s = strtok(NULL, ";");
		}
	}
	if (log_init(semi_cnt + 1, targets) < 0) {
		sfree(targets);
		quit(1);
	}
	sfree(targets);
	init_n++;

	if (lt_dlinit() != 0) {
		mlog(LEVEL_ERROR, "Failed to initialize libltdl");
		quit(1);
	}
	init_n++;

	if (eventloop_init() < 0) {
		mlog(LEVEL_ERROR, "Failed to initialize event loop");
		quit(1);
	}
	init_n++;

	plugin_init();
	init_n++;

	if (config.plugin.plugins[0] != 0) {
		char *s = strtok(config.plugin.plugins, ";");
		while (s != NULL) {
			if (plugin_load(s) < 0) {
				quit(1);
			}
			s = strtok(NULL, ";");
		}
	}

	plugin_finalize_load();

	if (db_open_global() < 0) {
		mlog(LEVEL_ERROR, "Failed to open database");
		quit(1);
	}
	init_n++;

	switch (mode) {
	case 0:
		mlog(LEVEL_NORMAL, "Starting HTTP server on %s:%d",
		     config.server.address, config.server.port);
		eventloop_start();
		break;
	case 1:
		do_migrate();
		break;
	}

	mlog(LEVEL_NORMAL, "Bye!");

	quit(0);
	return 0;
}
