#include <sqlite3.h>
#include <stdlib.h>
#include <string.h>

#include "sql.h"
#include "trie.h"
#include "util.h"

struct idbc {
	sqlite3 *db;
	struct trie *prepared;
};

struct idb_cursor {
	sqlite3_stmt *stmt;
	int last_ret;
	size_t max_binding;
	struct db_param **bindings;
};

static void free2(void *a, void *b) {
	sfree(a);
}

static struct idbc *sqlite_db_open(const char *filename) {
	struct idbc *idbc = salloc(1, sizeof(struct idbc));
	idbc->prepared = trie_new();
	if (sqlite3_open(filename, &idbc->db) != SQLITE_OK) {
		trie_free(idbc->prepared, NULL, NULL);
		sfree(idbc);
		return NULL;
	}
	return idbc;
}

static void sqlite_db_close(struct dbc *dbc) {
	trie_free(dbc->idbc->prepared, free2, NULL);
	sqlite3_close(dbc->idbc->db);
	sfree(dbc->idbc);
}

static int sqlite_db_create_prepared(struct dbc *dbc, const char *name, const char *stmt) {
	char *copy = sstrdup(stmt);
	if (trie_add(dbc->idbc->prepared, name, copy) < 0) {
		sfree(copy);
		return -1;
	}
	return 0;
}

static int sqlite_db_delete_prepared(struct dbc *dbc, const char *name) {
	return trie_delete(dbc->idbc->prepared, name, free2, NULL);
}

static struct idb_cursor *sqlite_db_exec_direct(struct dbc *dbc, const char *stmt,
                                                size_t params_num, struct db_param *params);

static struct idb_cursor *sqlite_db_exec_prepared(struct dbc *dbc, const char *name,
                                                  size_t params_num, struct db_param *params) {
	void *stmt;
	if (trie_search(dbc->idbc->prepared, name, &stmt) < 0) {
		return NULL;
	}
	return sqlite_db_exec_direct(dbc, stmt, params_num, params);
}

static struct idb_cursor *sqlite_db_exec_direct(struct dbc *dbc, const char *stmt,
                                                size_t params_num, struct db_param *params) {
	struct idb_cursor *icur = salloc(1, sizeof(struct idb_cursor));

	sqlite3_stmt *stmtp;
	if (sqlite3_prepare_v2(dbc->idbc->db, stmt, strlen(stmt) + 1, &stmtp, NULL) != SQLITE_OK) {
		sfree(icur);
		return NULL;
	}

	for (size_t i = 0; i < params_num; i++) {
		int n = SQLITE_ERROR;
		switch (params[i].type) {
		case SQL_NULL:
			n = sqlite3_bind_null(stmtp, i + 1);
			break;
		case SQL_INTEGER:
			n = sqlite3_bind_int(stmtp, i + 1, params[i].value.num);
			break;
		case SQL_TEXT:
			n = sqlite3_bind_text(stmtp, i + 1, params[i].value.str, -1,
			                      SQLITE_TRANSIENT);
			break;
		}
		if (n != SQLITE_OK) {
			sqlite3_reset(stmtp);
			sqlite3_finalize(stmtp);
			sfree(icur);
			return NULL;
		}
	}

	icur->last_ret = sqlite3_step(stmtp);
	if (icur->last_ret != SQLITE_DONE && icur->last_ret != SQLITE_ROW) {
		sqlite3_reset(stmtp);
		sqlite3_finalize(stmtp);
		sfree(icur);
		return NULL;
	}

	icur->stmt = stmtp;
	return icur;
}

static void sqlite_db_cursor_free(struct db_cursor *cur) {
	sfree(cur->icur->bindings);
	sqlite3_reset(cur->icur->stmt);
	sqlite3_finalize(cur->icur->stmt);
}

static int sqlite_db_cursor_bind(struct db_cursor *cur, int column, struct db_param *param) {
	if (column < 0) {
		return -1;
	}
	if (column + 1 > cur->icur->max_binding) {
		cur->icur->bindings = srealloc(cur->icur->bindings,
		                               column + 1, sizeof(struct db_param*));
		cur->icur->max_binding = column + 1;
	}
	cur->icur->bindings[column] = param;
	return 0;
}

static int sqlite_db_cursor_end(struct db_cursor *cur) {
	return cur->icur->last_ret == SQLITE_DONE;
}

static int sqlite_db_cursor_next(struct db_cursor *cur) {
	if (cur->icur->max_binding > sqlite3_column_count(cur->icur->stmt)) {
		return -1;
	}
	for (size_t i = 0; i < cur->icur->max_binding; i++) {
		int type = sqlite3_column_type(cur->icur->stmt, i);
		switch (type) {
		case SQLITE_NULL:
			cur->icur->bindings[i]->type = SQL_NULL;
			break;
		case SQLITE_INTEGER:
			cur->icur->bindings[i]->type = SQL_INTEGER;
			cur->icur->bindings[i]->value.num = sqlite3_column_int(cur->icur->stmt, i);
			break;
		case SQLITE_TEXT:
			cur->icur->bindings[i]->type = SQL_TEXT;
			cur->icur->bindings[i]->value.str =
				(const char*)sqlite3_column_text(cur->icur->stmt, i);
			break;
		}
	}

	cur->icur->last_ret = sqlite3_step(cur->icur->stmt);
	if (cur->icur->last_ret != SQLITE_DONE && cur->icur->last_ret != SQLITE_ROW) {
		return -1;
	}
	return 0;
}

void plugin_init() {
	struct db_backend backend = {
		.f_db_open = sqlite_db_open,
		.f_db_close = sqlite_db_close,
		.f_db_create_prepared = sqlite_db_create_prepared,
		.f_db_delete_prepared = sqlite_db_delete_prepared,
		.f_db_exec_prepared = sqlite_db_exec_prepared,
		.f_db_exec_direct = sqlite_db_exec_direct,
		.f_db_cursor_free = sqlite_db_cursor_free,
		.f_db_cursor_bind = sqlite_db_cursor_bind,
		.f_db_cursor_end = sqlite_db_cursor_end,
		.f_db_cursor_next = sqlite_db_cursor_next,
	};
	db_register_backend("sqlite", &backend);
}
